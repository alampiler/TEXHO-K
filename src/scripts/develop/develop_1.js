


$(document).ready(function() {
  let animationItem = $('.loans-content-block');
  let form = $('.request-form');
  $('.main > button').remove();
  $('.main > ul').remove();

  function inputSettings(){
    $('input[type="text"]').on('click', function () {
      $(this).attr('placeholder', 'Например Максим')
    });

    $('input[type="text"]').on('blur', function () {
      $(this).attr('placeholder', 'Введите имя')
    });
  }

  function modalSettings(){
    $('.l-menu-open-btn').click(function () {
      $('.l-menu-mobile').fadeToggle(300);
      $('.l-menu-mobile').css('display', 'flex');
      $('.l-menu-open-btn').toggleClass('active');
      $('.header-services').fadeOut(300);
      $('.class-services-active').fadeOut(300);
    });

    $('.services').click(function () {
      $('.header-services').fadeIn(300);
      $('.class-services-active').fadeIn(300)
    });

    $('.header-services > .close').click(function () {
      $('.header-services').fadeOut(300);
      $('.class-services-active').fadeOut(300);
    });
  }

  function slidersSettings(){
    $('.top-section-slider').slick({
      autoplay: false,
      autoplaySpeed: 2000,
      arrows: true,
      dots: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      swipe: false,
      prevArrow: $('.slider-prev'),
      nextArrow: $('.slider-next')
    });

    $('.services-and-solutions-slider').slick({
      slidesToShow: 4,
      slidesToScroll: 1,
      infinite: false,
      dots: true,
      swipe: false,
      responsive: [
        {
          breakpoint: 1156,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            swipe: true
          }
        },
        {
          breakpoint: 523,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            swipe: true
          }
        }
      ]
    });

    $('.customers-slider').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      vertical: true,
      verticalSwiping: true,
      dots: true,
      arrows: true,
      infinite: false,
      swipe: false,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            swipe: true
          }
        },
      ]
    })
  }

  validate(form);

  animationBlock(animationItem);
  inputSettings();
  modalSettings();
  slidersSettings();
});

$(window).load(function(){

});

$(window).resize(function(){

});
